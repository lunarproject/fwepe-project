<?php
/*
 * Copyright (c) 20122012 Farly <farly@lunarproject.org>.
 *
 * This file is part of FWEPE 3.0.
 *
 * FWEPE 3.0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FWEPE 3.0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FWEPE 3.0.  If not, see <http ://www.gnu.org/licenses/>.
 */

$cfg['Databases']['Default'] = array(
    'dsn' => 'mysql:host=127.0.0.1;dbname=fwepe;charset=utf8;',
    'user' => 'root',
    'pass' => 'root123root'
);

/*** End: Database.php ***/
