<?php
/*
 * Router.conf.php
 *
 * Copyright (c) 2012 Farly <farly@lunarproject.org>
 *
 * This file is part of FWEPE 3.
 *
 * FWEPE 3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FWEPE 3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FWEPE 3.  If not, see <http ://www.gnu.org/licenses/>.
 */

$cfg['Blueprint'] = array(
    array('/', 'Controller\Home::index', 'home_index'),
    array('/hello/{1}', 'Controller\Home::hello', 'home_hello'),
    array('/view', 'Controller\Home::view', 'home_view'),
    array('/news', 'Controller\News::index', 'news_index'),
    array('/news/add', 'Controller\News::add', 'news_add'),
    array('/news/{1}/edit', 'Controller\News::edit', 'news_edit'),
    array('/news/{1}/delete', 'Controller\News::delete', 'news_delete')
);
/*** End: Router.conf.php ***/
