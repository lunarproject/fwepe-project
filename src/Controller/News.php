<?php

namespace Controller;

use Fwepe\Factory\MVC\Controller;
use Model\NewsModel;

class News extends Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->newsModel = new NewsModel();
    }
    
    public function index()
    {
        $context['news'] = $this->newsModel->getAll();
        echo $this->twig->render('index.html', $context);
    }
        
    public function add()
    {
        $title = $this->request->post('title');
        $content = $this->request->post('content');
        
        $this->newsModel->insert($title, $content);
        
        $context['content'] = 'Insert success';
        echo $this->twig->render('message.html', $context);
    }

}
