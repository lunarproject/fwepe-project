<?php

namespace Model;

use \Fwepe\Factory\MVC\Model;

class NewsModel extends Model
{
    public function insert($title, $content)
    {
        $query = "INSERT INTO news(title, content, publish_date)
            VALUES(?, ?, ?)";
        
        $publishDate = date('Y-m-d H:i:s');
        $sth = $this->db->prepare($query);
        
        $sth->bindValue(1, $title, \PDO::PARAM_STR);
        $sth->bindValue(2, $content, \PDO::PARAM_STR);
        $sth->bindValue(3, $publishDate, \PDO::PARAM_STR);
        $sth->execute();
    }
    
    public function getAll()
    {
        $query = "SELECT * FROM news ORDER BY publish_date DESC LIMIT 0, 10";
        
        $sth = $this->db->prepare($query);
        
        $sth->execute();
        $results = $sth->fetchAll(\PDO::FETCH_ASSOC);
        
        return $results;
    }
}
